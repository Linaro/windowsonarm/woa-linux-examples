#include <cstdlib>
#include <windows.h>

#include <cassert>
#include <iostream>
#include <thread>

void closeWindow(const char *title) {
  for (;;) {
    std::cout << "Wait 2 seconds before closing window\n";
    Sleep(2000);
    auto w = FindWindow(NULL, title);
    if (w) {
      std::cout << "Closing window \"" << title << "\"\n";
      auto posted = PostMessage(w, WM_CLOSE, 0, 0);
      assert(posted);
      return;
    }
  }
}

int main() {
  const char *message = "Hello World";
  std::thread closer(closeWindow, message);
  int ret = MessageBox(NULL, message, message, MB_OK);
  if (ret == -1)
  {
      std::cout << "MessageBox failed, is there a graphical server running?\n";
      exit(EXIT_FAILURE);
  }
  assert(ret == IDCANCEL);
  std::cout << "MessageBox was correctly closed\n";
  closer.join();

  return 0;
}
